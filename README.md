Exablack Docker Images
==========
[![License (LGPL version 3)](https://img.shields.io/badge/license-GNU%20LGPL%20version%203.0-blue.svg?maxAge=2592000)](https://bitbucket.com/exablackti/docker-images/blob/master/LICENCE)

Exablack TI Docker images repository. You can see public registries at [Exablack TI Public Repositories](https://hub.docker.com/u/exablack/)

Available images
---

+ [`apache-synapse`][apache-synapse]: [Apache Synapse][apache-synapse-site] image
+ [`wso2-base`][wso2-base]: base image that have basic configuration and utility libraries used by many images
+ [`wso2-apim`][wso2-apim]: [WSO2 API Manager][wso2-apim-site] image
+ [`wso2-as`][wso2-as]: [WSO2 Application Server][wso2-as-site] image
+ [`wso2-dss`][wso2-dss]: [WSO2 Data Services Server][wso2-dss-site] image
+ [`wso2-esb`][wso2-esb]: [WSO2 Enterprise Service Bus][wso2-esb-site] image
+ [`wso2-greg`][wso2-greg]: [WSO2 Governance Registry][wso2-greg-site] image
+ [`wso2-is`][wso2-is]: [WSO2 Identity Server][wso2-is-site] image

License
---

This project and its documentation are licensed under the LGPL license. Refer to [LICENCE](LICENCE) for more information.

[apache-synapse]: https://bitbucket.org/exablack/docker-images/src/master/apache-synapse
[apache-synapse-site]: http://synapse.apache.org/
[wso2-base]: https://bitbucket.org/exablackti/docker-images/src/master/wso2-base
[wso2-apim]: https://bitbucket.org/exablack/docker-images/src/master/wso2-am
[wso2-apim-site]: http://wso2.com/api-management/
[wso2-as]: https://bitbucket.org/exablackti/docker-images/src/master/wso2-as
[wso2-as-site]: http://wso2.com/products/application-server/
[wso2-dss]: https://bitbucket.org/exablackti/docker-images/src/master/wso2-dss
[wso2-dss-site]: http://wso2.com/products/data-services-server/
[wso2-esb]: https://bitbucket.org/exablackti/docker-images/src/master/wso2-esb
[wso2-esb-site]: http://wso2.com/products/enterprise-service-bus/
[wso2-greg]: https://bitbucket.org/exablackti/docker-images/src/master/wso2-greg
[wso2-greg-site]: http://wso2.com/products/governance-registry/
[wso2-is]: https://bitbucket.org/exablackti/docker-images/src/master/wso2-is
[wso2-is-site]: http://wso2.com/products/identity-server/
