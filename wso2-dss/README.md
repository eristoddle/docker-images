WSO2 Data Services Server Docker Image
==========
[![License (LGPL version 3)](https://img.shields.io/badge/license-GNU%20LGPL%20version%203.0-blue.svg?maxAge=2592000)](https://bitbucket.com/exablackti/docker-images/blob/master/LICENCE)

This image use the embedded H2 database to persist carbon stuff. You need to extend - or change and commit - this image to change default configurations.

[`exablack/wso2-base:1.0.0`](https://hub.docker.com/r/exablack/wso2-base/) is the base image of this image.

Supported tags and Dockerfile links
---

- `latest`, `3.5.0` [(Dockerfile)](https://bitbucket.com/exablackti/docker-images/src/HEAD/wso2-dss/Dockerfile)
- `3.2.2` [(Dockerfile)](https://bitbucket.com/exablackti/docker-images/src/HEAD/wso2-dss/Dockerfile)

Utilities?
---
To simplify use off another RDBMS, such as PostgreSQL and MySQL, JDBC connectors for this SGBDs are available at `${CARBON_HOME}/repository/components/lib/`.

How to use?
---

The following ports should be published:

- 9443
- 9763

You can create a container from this image running something like this:

```
#!shell-session
docker run -d -p 9443:9443 -p 9763:9763 exablack/wso2-dss
```

License
---

This project and its documentation are licensed under the LGPL license. Refer to [LICENCE](LICENCE) for more information.
