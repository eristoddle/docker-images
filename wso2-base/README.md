WSO2 Docker base Image
==========
[![License (LGPL version 3)](https://img.shields.io/badge/license-GNU%20LGPL%20version%203.0-blue.svg?maxAge=2592000)](https://bitbucket.com/exablackti/docker-images/blob/master/LICENCE)

WSO2 base image with Oracle JDK `7u79` installed and `JAVA_HOME` exported. Also, JDBC connectors for
PostgreSQL and MySQL are available at `/opt/utilities/`.

[`centos:7`](https://hub.docker.com/_/centos/) is the base image of this image.

Supported tags and Dockerfile links
---

- `latest`, `1.0.0` [(Dockerfile)](https://bitbucket.com/exablackti/docker-images/src/e711b52b/wso2-base/Dockerfile) - [![](https://imagelayers.io/badge/exablack/wso2-base:latest.svg)](https://imagelayers.io/?images=exablack/wso2-base:latest 'Get your own badge on imagelayers.io')

How to use?
---

As a base image this not intend to create running container, bui you can do something like this:

```
#!shell-session
docker run --rm -it exablack/wso2-base bash
```

To extend this image, simple add this to you `Dockerfile`

```
#!shell-session
FROM exablack/wso2-base:1.0.0
```

License
---

This project and its documentation are licensed under the LGPL license. Refer to [LICENCE](LICENCE) for more information.
