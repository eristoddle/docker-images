Apache Synapse Docker Image
==========
[![License (LGPL version 3)](https://img.shields.io/badge/license-GNU%20LGPL%20version%203.0-blue.svg?maxAge=2592000)](https://bitbucket.com/exablackti/docker-images/blob/master/LICENCE) [![](https://images.microbadger.com/badges/image/exablack/apache-synapse.svg)](https://microbadger.com/images/exablack/apache-synapse "Badge by microbadger.com")

You need to extend - or change and commit - this image to change default configurations.

[`centos:7`](https://hub.docker.com/_/centos/) is the base image of this image.

Supported tags and Dockerfile links
---

- `latest`, `2.1.0` [(Dockerfile)](https://bitbucket.com/exablackti/docker-images/src/HEAD/apache-synapse/Dockerfile)

How to use?
---

The following ports may be published:

- 8243
- 8280

You can create a container from this image running something like this:

```
#!shell-session
docker run -d -p 8243:8243 -p 8280:8280 exablack/apache-synapse
```

License
---

This project and its documentation are licensed under the LGPL license. Refer to [LICENCE](LICENCE) for more information.
